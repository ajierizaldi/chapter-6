require('dotenv').config();
const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const bodyParser = require('body-parser');
const cors = require('cors');
const router = require('./routes');
const YAML = require('yamljs');
const swaggerUi = require('swagger-ui-express');
const apiDocs = YAML.load('./swagger.yaml');

app.use(bodyParser.json());
app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use('/api', router);
app.use('/apiDocs', swaggerUi.serve, swaggerUi.setup(apiDocs));

app.listen(port, () => console.log(`Server is running on port ${port}`));