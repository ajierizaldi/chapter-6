const express = require('express');
const router = express.Router();
const carRoutes = require('./carRoutes');
const userRoutes = require('./user');

router.get('/cekOmbak', (req, res) => res.send('Hello World!'));
router.use('/cars', carRoutes);
router.use('/user', userRoutes);

module.exports = router;