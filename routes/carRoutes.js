const express = require('express');
const router = express.Router();
const { list, create, update, destroy } = require('../controllers/carController');
const validate = require('../middleware/validate');
const { createCarRules } = require('../validators/rule');
const { tokenChecker } = require('../middleware/tokenChecker');

router.get('/list', tokenChecker, list);
router.post('/create', tokenChecker, validate(createCarRules), create);
router.put('/update', tokenChecker, update);
router.delete('/destroy', tokenChecker, destroy);

module.exports = router