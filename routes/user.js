const express = require('express');
const router = express.Router();
const { register, login } = require('../controllers/userController');
const { registerRules } = require('../validators/rule');
const validate = require('../middleware/validate');

router.post('/register', validate(registerRules), register);
router.post('/login', login);

module.exports = router