const jwt = require('jsonwebtoken');

const tokenChecker = (req, res, next) => {
    let token = req.headers.authorization;

    if (!token) {
        return res.status(403).json({
            "success": false,
            "error": 403,
            "message": 'No token provided',
            "data": null
        });
    }

    // Remove Bearer from token
    if (token.lowerCase().startsWith('bearer')) {
        token = token.slice('bearer'.length).trim();
    }

    try {
        const jwtPayload = jwt.verify(token, 'password!23');

        if (!jwtPayload) {
            return res.status(403).json({
                "success": false,
                "error": 403,
                "message": 'Invalid token',
                "data": null
            });
        }

        res.locals.user = jwtPayload;
        next();
        
    } catch (error) {
        return res.status(403).json({
            "success": false,
            "error": 403,
            "message": 'Invalid token',
            "data": null
        });
    }
}