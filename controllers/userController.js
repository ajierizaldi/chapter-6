const { user } = require('pg/lib/defaults');
const model = require('../models'),
    { genSalt, hash, compareSync } = require('bcrypt'),
    jwt = require('jsonwebtoken')

const cryptPassword = async (password) => {
    const salt = await genSalt(12);

    return hash(password, salt);
}

module.exports = {
    register: async (req, res) => {
        try {
            const data = await model.user.create({
                ...req.body,
                password: await cryptPassword(req.body.password)
            });

            return res.status(200).json({
                "success" : true,
                "error" : 0,
                "message" : "user successfully registered",
                "data" : data
            })
        } catch (error) {
            return res.status(500).json({
                "success" : false,
                "error" : error.code,
                "message" : error,
                "data" : null
            });
        }
    },
    login: async (req, res) => {
        try {
            const data = await model.user.findOne({
                where: {
                    email: req.body.username
                }
            });

            if(!userExists) {
                return res.status(404).json({
                    "success" : false,
                    "error" : 404,
                    "message" : "user not found",
                    "data" : null
                })
            }

            if(!compareSync(req.body.password, userExists.password)) {
                const token = jwt.sign(
                    { id: userExists.id, username: userExists.username, email: userExists.email }, 'password!23',
                    { expiresIn: '1h' }
                )
            }

            return res.status(200).json({
                "success" : true,
                "error" : 0,
                "message" : "user successfully logged in",
                "data" : {
                    "token" : token
                }
            })

        } catch (error) {
            return res.status(500).json({
                "success" : false,  
                "error" : error.code,
                "message" : error,
                "data" : null
            });
        }
    }
}